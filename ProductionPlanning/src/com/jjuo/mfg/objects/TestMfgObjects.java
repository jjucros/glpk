package com.jjuo.mfg.objects;

/**
 * Created by Ze on 1/2/2015.
 */
public class TestMfgObjects {
    public static void main(String[] args) {
        MfgMaterial M = new MfgMaterial(1, "Madera 1010", "Comprada");
        M.outMfgMaterial();

        Location L = new Location(1, "Bogota", 1000);
        Location L1 = new Location(2, "Medellin");

        L.outLocation();
        L1.outLocation();

        Warehouse W = new Warehouse(1, "Bogota-Fontibon", L);
        W.outWarehouse();

        Sku S = new Sku(1, L1, M);
        S.outSku();


    }


}
