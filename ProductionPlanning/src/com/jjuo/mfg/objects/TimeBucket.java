package com.jjuo.mfg.objects;

/**
 * Created by Ze on 1/2/2015.
 */
public class TimeBucket {
    private int ID;
    private String Name;
    private String tbType;

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setTBType(int t) {
        switch (t) {
            case 1:
                tbType = "Year";
            case 2:
                tbType = "Month";
            case 3:
                tbType = "Day";
            case 4:
                tbType = "Day";
            default:
                tbType = "Invalid TimeBucket";
        }
    }
}


