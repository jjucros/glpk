package com.jjuo.mfg.objects;

import javax.swing.*;

/**
 * Created by Ze on 1/2/2015.
 */
public class Warehouse {
    private int ID;
    private String Name;
    private int ParentLocationID;
    private String ParentLocationName;


    public Warehouse(int ID, String Name, Location parentL) {
        this.setID(ID);
        this.setName(Name);
        this.setParentLocationID(parentL);
        this.setParentLocationName(parentL);
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setParentLocationID(Location parentL) {
        this.ParentLocationID = parentL.getID();
    }

    public void setParentLocationName(Location parentL) {
        this.ParentLocationName = parentL.getName();
    }

    public void outWarehouse() {
        JOptionPane.showMessageDialog(null, "Warehouse ID: \t" + this.ID + "\nWarehouse Name: \t" + this.Name + "\nWarehouse ParentLocation: \t" + this.ParentLocationID + "-" + this.ParentLocationName);
    }

}
