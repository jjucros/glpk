package com.jjuo.mfg.objects;

import javax.swing.*;

/**
 * Created by Ze on 1/2/2015.
 */
public class Location {
    private int ID;
    private String Name;
    private double MaxCapacity;

    public Location(int ID, String Name, double MaxCapacity) {
        this.setID(ID);
        this.setName(Name);
        this.setMaxCapacity(MaxCapacity);
    }

    public Location(int ID, String Name) {
        this.setID(ID);
        this.setName(Name);
        this.setMaxCapacity(Double.MAX_VALUE);
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setMaxCapacity(double maxCapacity) {
        this.MaxCapacity = maxCapacity;
    }

    public void outLocation() {
        JOptionPane.showMessageDialog(null, "Location ID: \t" + this.ID + "\nLocation Name: \t" + this.Name + "\nLocation Max Capacity: \t" + this.MaxCapacity);
    }

    public int getID() {
        return this.ID;
    }

    public String getName() {
        return this.Name;
    }
}