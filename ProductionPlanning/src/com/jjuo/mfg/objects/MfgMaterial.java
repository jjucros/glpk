package com.jjuo.mfg.objects;

import javax.swing.*;

/**
 * Created by Ze on 1/2/2015.
 */
public class MfgMaterial {
    private int ID;
    private String Name;
    private String matType;

    public MfgMaterial(int ID, String Name, String matType) {
        this.setId(ID);
        this.setName(Name);
        this.setMatType(matType);
    }

    public void setId(int ID) {
        this.ID = ID;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setMatType(String matType) {
        this.matType = matType;
    }

    public void outMfgMaterial() {
        JOptionPane.showMessageDialog(null, "MfgMaterial ID: \t" + this.ID + "\nMfgMaterial Name: \t" + this.Name + "\nMfgMaterial Type: \t" + this.matType);
    }

    public String getName() {
        return this.Name;
    }

}
