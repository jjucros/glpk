package com.jjuo.mfg.objects;

import javax.swing.*;

/**
 * Created by Ze on 1/2/2015.
 */
public class Sku {
    private int ID;
    private String Name;
    private MfgMaterial parentMaterial;
    private Location parentLocation;

    public Sku(int ID, Location parentLocation, MfgMaterial parentMaterial) {
        this.setID(ID);
        this.setParentLocation(parentLocation);
        this.setParentMaterial(parentMaterial);
        this.setName(parentLocation, parentMaterial);
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setParentLocation(Location parentLocation) {
        this.parentLocation = parentLocation;
    }

    public void setParentMaterial(MfgMaterial parentMaterial) {
        this.parentMaterial = parentMaterial;
    }

    public void setName(Location parentLocation, MfgMaterial parentMaterial) {
        this.Name = this.parentLocation.getName() + "~" + this.parentMaterial.getName();
    }

    public void outSku() {
        JOptionPane.showMessageDialog(null, "Sku ID: \t" + this.ID + "\nSku Name: \t" + this.Name + "\nSku Location: \t" + this.parentLocation.getName());
    }

    public String getName() {
        return this.Name;
    }

}
