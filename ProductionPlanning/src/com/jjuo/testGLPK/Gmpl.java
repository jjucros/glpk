package com.jjuo.testGLPK;

import com.jjuo.gui.SetLooAndkFeel;
import org.gnu.glpk.*;

import javax.swing.*;
import java.io.File;

public class Gmpl implements GlpkCallbackListener, GlpkTerminalListener {
    private boolean hookUsed = false;

    public static void main(String[] arg) {

        SetLooAndkFeel.SetLAF();

        if (1 != arg.length) {
            System.out.println("Start....");
            //return;
        }


        /**
         * Used to set the locale for numeric formatting. When importing model files the GLPK library expects to be using locale "C"
         */
        GLPK.glp_java_set_numeric_locale("C");
        //new Gmpl().solve(arg);
        String[] file = new String[1];
        //file[0] = "C://GLPK//examples//java//marbles.mod";

        JFileChooser chFile = new JFileChooser("C://gusek//examples//csv_jjuo");
        chFile.setDialogTitle("Seleccione el archivo del Modelo (*.mod)");
        chFile.setVisible(true);
        int returnValue = chFile.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = chFile.getSelectedFile();
            //System.out.println(selectedFile.getName());
            file[0] = selectedFile.getAbsolutePath();
            System.out.println(file[0]);
        }
        else
            return;


        //file[0] = "C://GLPK//examples//java//huge.mod";


        new Gmpl().solve(file);
    }

    /**
     * Método para resolver el archivo de modelo (*.mod) que se pasa como parámetro
     * @param arg
     */
    public void solve(String[] arg) {
        glp_prob lp = null;
        glp_tran tran;
        glp_iocp iocp;

        String fname;
        int skip = 0;
        int ret;

        // listen to callbacks
        GlpkCallback.addListener(this);

        // listen to terminal output
        GlpkTerminal.addListener(this);

        fname = arg[0];

        lp = GLPK.glp_create_prob();
        System.out.println("Problem created");

        tran = GLPK.glp_mpl_alloc_wksp();
        ret = GLPK.glp_mpl_read_model(tran, fname, skip);
        if (ret != 0) {
            GLPK.glp_mpl_free_wksp(tran);
            GLPK.glp_delete_prob(lp);
            throw new RuntimeException("Model file not found: " + fname);
        }

        // generate model
        GLPK.glp_mpl_generate(tran, null);
        // build model
        GLPK.glp_mpl_build_prob(tran, lp);
        // set solver parameters
        iocp = new glp_iocp();
        GLPK.glp_init_iocp(iocp);
        iocp.setPresolve(GLPKConstants.GLP_ON);
        // do not listen to output anymore
        GlpkTerminal.removeListener(this);
        // solve model
        ret = GLPK.glp_intopt(lp, iocp);
        // postsolve model
        if (ret == 0) {
            GLPK.glp_mpl_postsolve(tran, lp, GLPKConstants.GLP_MIP);
        }
        // free memory
        GLPK.glp_mpl_free_wksp(tran);
        GLPK.glp_delete_prob(lp);

        // do not listen for callbacks anymore
        GlpkCallback.removeListener(this);

        // check that the hook function has been used for terminal output.
        if (!hookUsed) {
            System.out.println("Error: The terminal output hook was not used.");
            System.exit(1);
        }
    }

    @Override
    public boolean output(String str) {
        hookUsed = true;
        System.out.print(str);
        return false;
    }

    @Override
    public void callback(glp_tree tree) {
        int reason = GLPK.glp_ios_reason(tree);
        if (reason == GLPKConstants.GLP_IBINGO) {
            System.out.println("Better solution found");
        }
    }
}
