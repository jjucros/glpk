package com.jjuo.testGLPK;

import org.gnu.glpk.*;

import javax.swing.*;

/**
 * Created by Ze on 1/5/2015.
 */
public class TestMip {
    public static void main(String[] args) {
        int n;
        n = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el número de variables")) + 1;
        //n=10+1;

        String[] varNames = new String[n + 1];

        for (int i = 0; i < n + 1; i++) {
            varNames[i] = "X[" + i + "]";
            //System.out.println(varNames[i]);
        }

        //Éstas todavía no se para que son
        glp_prob lp;
        glp_iocp iocp;
        SWIGTYPE_p_int ind;
        SWIGTYPE_p_double val;
        int ret;

        // Crear el problema
        lp = GLPK.glp_create_prob();
        System.out.println("Problema creado");
        GLPK.glp_set_prob_name(lp, "ProblemaBasico");

        // Definir columnas/variables
        GLPK.glp_add_cols(lp, n);
        for (int i = 1; i < varNames.length; i++) {
            GLPK.glp_set_col_name(lp, i, varNames[i]);
            GLPK.glp_set_col_kind(lp, i, GLPKConstants.GLP_IV);
            GLPK.glp_set_col_bnds(lp, i, GLPKConstants.GLP_LO, 0, 0);
        }

        // Crear Restricciones
        GLPK.glp_add_rows(lp, 1);
        GLPK.glp_set_row_name(lp, 1, "c1");
        GLPK.glp_set_row_bnds(lp, 1, GLPKConstants.GLP_UP, 0, n * n);

        ind = GLPK.new_intArray(n + 1);
        val = GLPK.new_doubleArray(n + 1);


        for (int i = 1; i < varNames.length; i++) {
            GLPK.intArray_setitem(ind, i, i);
        }

        for (int i = 1; i < varNames.length; i++) {
            GLPK.doubleArray_setitem(val, i, 1);
        }
        GLPK.glp_set_mat_row(lp, 1, n - 1, ind, val);


        //Prueba de Otra restricción
        for (int r = 1; r < varNames.length - 1; r++) {
            GLPK.glp_add_rows(lp, 1);
            String Temp = "c_" + r;
            GLPK.glp_set_row_name(lp, r + 1, Temp);
            GLPK.glp_set_row_bnds(lp, r + 1, GLPKConstants.GLP_UP, 0, n / 2);

            /*
            System.out.println("El nombre de la restriccion es: \t" + GLPK.glp_get_row_name(lp, r+1));
            System.out.println("La cota inferior es: \t"+GLPK.glp_get_row_lb(lp, r+1));
            System.out.println("La cota superior es: \t"+GLPK.glp_get_row_ub(lp, r+1));
            */


            for (int i = 1; i < varNames.length; i++) {
                if (r == i) GLPK.intArray_setitem(ind, i, i);
            }

            for (int i = 1; i < varNames.length; i++) {
                if (r == i) {
                    GLPK.doubleArray_setitem(val, i, Math.random());
                } else {
                    GLPK.doubleArray_setitem(val, i, 0);
                }
            }
            //GLPK.glp_set_mat_row(lp, 2, n - 1, ind, val);
            GLPK.glp_set_mat_row(lp, r + 1, n, ind, val);
        }


        // Borrar los arrays para liberar memoria
        GLPK.delete_doubleArray(val);
        GLPK.delete_intArray(ind);

        //Definir la función Objetivo
        GLPK.glp_set_obj_name(lp, "FunObj");
        GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MAX);
        GLPK.glp_set_obj_coef(lp, 0, 0);

        for (int i = 1; i < n; i++) {
            GLPK.glp_set_obj_coef(lp, i, 1);
        }

        //Crear el archivo LP
        GLPK.glp_write_lp(lp, null, "C:\\Users\\Ze\\git\\glpk\\ProductionPlanning\\src\\com\\jjuo\\testGLPK\\ModelLP.lp");
        //  Resolvert el modelo
        iocp = new glp_iocp();
        GLPK.glp_init_iocp(iocp);
        iocp.setPresolve(GLPKConstants.GLP_ON);

        //  GLPK.glp_write_lp(lp, null, "yi.lp");
        ret = GLPK.glp_intopt(lp, iocp);

        //  Retrieve solution
        if (ret == 0) {
            write_mip_solution(lp);
        } else {
            System.out.println("The problem could not be solved");
        }
        ;

        // free memory
        GLPK.glp_delete_prob(lp);
    }

    static void write_mip_solution(glp_prob lp) {
        int i;
        int n;
        String name;
        double val;

        name = GLPK.glp_get_obj_name(lp);
        val = GLPK.glp_mip_obj_val(lp);
        System.out.print(name);
        System.out.print(" = ");
        System.out.println(val);
        n = GLPK.glp_get_num_cols(lp);
        for (i = 1; i < n; i++) {
            name = GLPK.glp_get_col_name(lp, i);
            val = GLPK.glp_mip_col_val(lp, i);
            System.out.print(name);
            System.out.print(" = ");
            System.out.println(val);
        }
    }

}
